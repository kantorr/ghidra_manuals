#! /usr/bin/env python3

'''
Script to collect manuals for processors supported by ghidra.

Date:
	26 April 2021
	03 Januar 2024

Author:
	kantorr

Note:
	Based on: https://gist.github.com/alexmaloteaux/88a8aaa4c00c87e4532b4b2ff474d6e1
'''

import os, sys
#import requests
import traceback
from shutil import copyfile
from hashlib import md5
import binascii
import json

from support import LogStdIO as log
from support import StreamSimpleHuman as wget
from support import StreamError

# -----------------------------------------------------------------------------
def print_usage():
	print(f"Usage :{sys.argv[0]} <ghidra_base_folder> [-f]")

# -----------------------------------------------------------------------------
def md5_string(filename):
	return binascii.hexlify(md5(open(filename, "rb").read()).digest()).decode()

# -----------------------------------------------------------------------------
def match_md5_string(filename, hashstr):
	return md5_string(filename) == hashstr

# -----------------------------------------------------------------------------
def match_md5(lhsfile, rhsfile):
	lhs_md5 = md5(open(lhsfile, "rb").read())
	rhs_md5 = md5(open(rhsfile, "rb").read())
	return lhs_md5 == rhs_md5

# -----------------------------------------------------------------------------
def download_file(title, url, fname, md5str) :
	try:
		print('Downloading: "{}"'.format(title))
		print(' Source URL: {}'.format(url))
		print('  Saving as: {}'.format(fname))

		wget(url, fname).download()

		if not match_md5_string(fname, md5str):
			raise ValueError("Wrong file: md5 hash of downloaded file does not match")

	except StreamError as e:
		log.error('Could not download...')
		log.error('\tSource  URL: {}'.format(url))
		log.error('\tDestination: {}'.format(fname))
		log.error('{}'.format(str(e)))
		print('\n')
		# traceback.print_exc()
		if os.path.exists(fname):
			os.unlink(fname)
		return False
	
	return True

# *****************************************************************************
if __name__ == "__main__":
	import argparse
	arg_description = "Collecting manuals for processors supported by ghidra."
	arg_parser = argparse.ArgumentParser("get_ghidra_manuals.py", description=arg_description)
	arg_parser.add_argument("ghidra_folder", help="The folder of ghidra release (~/ghidra_11.0_PUBLIC).")
	arg_parser.add_argument("-f", "--force", help="Rewrite ghidra manuals although existing files.", action="store_true")
	args = arg_parser.parse_args()

	ghidra_folder = os.path.join(args.ghidra_folder, "Ghidra")
	force_rewrite = args.force

	if not os.path.exists(ghidra_folder):
		log.error(f"ghidra folder not found : {ghidra_folder}")
		sys.exit()

	try:
		manuals_db = None
		with open("db.json") as f:
			manuals_db = json.load(f)

		version_db = manuals_db["version"]
		version_ghidra = ""
		app_prop = os.path.join(ghidra_folder, "application.properties")

		if not os.path.exists(app_prop):
			log.warning("application.properties file not found, ghidra version may not be compatible")
		else:
			for line in open(app_prop, "r"):
				if line.startswith("application.version="):
					version_ghidra = line.split("=")[1].strip()
					if version_ghidra != version_db:
						log.error(f"Untested version of ghidra: {version_ghidra} Expected: {version_db}")
					else:
						log.success(f"Detected correct version of ghidra: {version_ghidra}")

		basepath = os.path.dirname(__file__)
		cache = os.path.join(basepath, "cache")
	
		if not os.path.exists(cache):
			os.mkdir(cache)

		for l in manuals_db.keys():          
			if type(manuals_db[l]) is not list:
				log.accent_strong(f"DB: {l}")
				log.accent_light(manuals_db[l])
				continue

			log.accent_strong(f"Processor : {l}")

			for manual_record in manuals_db[l]:
				pdf_name = manual_record['pdf']
				pdf_cache = os.path.join(cache, pdf_name)
				pdf_ghidra= os.path.join(ghidra_folder, "Processors", l, "data/manuals", pdf_name)
				idx_ghidra = os.path.join(ghidra_folder, "Processors", l, "data/manuals", manual_record['idx'])

				log.accent_light(f'ghidra index: "{idx_ghidra}"')
		
				if not os.path.exists(idx_ghidra):
					log.error(f"Index file not found: {idx_ghidra}, skipping")
					continue
		
				#@SPARCV9.pdf[The SPARC Architecture Manual, Version 9 (SAV09R1459912)]
				pdf_header = open(idx_ghidra, "r", encoding="cp1252").readline().split('[')[0][1:].strip()
				title_name = open(idx_ghidra, "r", encoding="cp1252").readline().split('[')[1].replace(']','').strip()
		
				log.accent_light(f'      Manual: "{title_name}"')
				log.accent_light(f'    PDF file: "{pdf_name}"')

				if pdf_name != pdf_header:
					log.error("Manual file in index is different:")
					log.error(f"\t     idx: {pdf_header}")
					log.error(f"\texpected: {pdf_name}")
					continue

				if os.path.exists(pdf_ghidra):
					if not match_md5_string(pdf_ghidra, manual_record['md5']):
						log.warning(f"Wrong existing manual, md5 mismatch: {pdf_ghidra}")
						log.warning("\tcalculated md5: {}".format(md5_string(pdf_ghidra)))
						log.warning("\t  expected md5: {}".format(manual_record['md5']))
						os.unlink(pdf_ghidra)
					elif force_rewrite:
						os.unlink(pdf_ghidra)
					else:
						log.warning(f"Skipping existing: {pdf_ghidra}")
						continue
		
				# Check the previously downloded file
				if os.path.exists(pdf_cache):
					if not match_md5_string(pdf_cache, manual_record['md5']):
						log.warning(f"Wrong file in cache, md5 mismatch: {pdf_cache}")
						log.warning("\tcalculated md5: {}".format(md5_string(pdf_cache)))
						log.warning("\t  expected md5: {}".format(manual_record['md5']))
						os.unlink(pdf_cache)
					else:
						print(f"Using previously downloaded: ./cache/{pdf_name}")
				
				# Download not-cached manual-file
				if not os.path.exists(pdf_cache):
					url = manual_record['src']
				
					if 'auth' in manual_record.keys() and manual_record['auth']:
						log.error(f'Authentication is required to download: "{title_name}"')     
						log.error(f'\tDownload it from: {url}')     
						log.error(f'\t         Save as: ./cache/{pdf_name}')     
						continue
				
					if not download_file(title_name, url, pdf_cache, manual_record['md5']):
						log.error(f"Error downloading manual: {url}")
						continue

				copyfile(pdf_cache, pdf_ghidra)
				log.success(f"Manual deployed in: {pdf_ghidra}")
			
				if 'align' in manual_record and not manual_record['align']:
					log.warning("This version of the pdf doesn't align with the idx, please provide correct link if you find one maching!")

	except KeyboardInterrupt:
		log.warning("User interruption!\nGoodbye.")

	except Exception as e:
		log.error("Error in main, ..")
		log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()
		sys.exit()

