# ghidra_manuals

Script to collect manuals for processors supported by ghidra.

## Usage

```
./get_ghidra_manuals.py <ghidra_directory> [-f]
```

## DB file

Data base of all supported manuals is stored in `db.json` file.  
To use a different DB replace the `db.json` with it's analog. 

