#! /usr/bin/env python3

'''
Simple stream (file) downloading classes.

Date:
	26 April 2021
Author:
	kantorr
'''

import sys
import urllib.request

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StreamError(Exception):
	def __init__(self, msg):
		super().__init__(msg)

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StreamBasic:
	'''Basic downloader without progress messsage.'''

	# -------------------------------------------------------------------------
	def __init__(self, srcurl, dst, progress=None):
		self._src_url = srcurl
		self._dst = dst
		self._progress = progress

	#--------------------------------------------------------------------------
	def progress(self, current, total):
		'''Empty progress method.'''
		pass

	#--------------------------------------------------------------------------
	def download(self):
		'''Download file by URL given in constructor.'''
		# req = urllib.request.Request(
		# 	self._src_url, 
		# 	data=None, 
		# 	headers={ 'User-Agent': 'Wget/1.20.3 (linux-gnu)',
		# 				# 'Accept' : '*.*'
		# 	},
		# )
		# response = urllib.request.urlopen(req)
		try:
			response = urllib.request.urlopen(self._src_url)			
			if response is not None:
				out_file = open(self._dst,"wb")
				content_len =  response.getheader("Content-Length")
				if content_len is not None:
					self.read(response, int(content_len), out_file)
				else:
					self.read(response, None, out_file)

		except (urllib.error.URLError, TypeError, ValueError) as e:
			raise StreamError(str(e))

		# Prepare a new empty line
		if self._progress is not None:
			sys.stdout.write("\n")
			sys.stdout.flush()

	# -------------------------------------------------------------------------
	def read(self, response_obj, src_size, dst_file, block_size = 8192):
		'''Copy content of a remote file into a local file.'''
		if self._progress is not None: 
			self.progress(0, src_size)

		if block_size is None:
			buffer = response_obj.read()
			dst_file.write(buffer)

			if self._progress is not None: 
				self.progress(len(buffer), src_size)
		else:
			current_size = 0
			while True:
				buffer = response_obj.read(block_size)
				if not buffer: 
					break

				current_size += len(buffer)
				dst_file.write(buffer)

				if self._progress != None: 
					self.progress(current_size, src_size)

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StreamSimple(StreamBasic):
	'''Downloader with simple progress message.'''

	# -------------------------------------------------------------------------
	def __init__(self, srcurl, dst):
		super().__init__(srcurl, dst, True)

	#--------------------------------------------------------------------------
	def progress(self, current, total):
		'''Simple one-line progress output.'''
		if total is not None:
			percent = 100.0 * float(current) / float(total)
			sys.stdout.write(
				'\r' + 80 * " " + '\r' +
				str(current) + " / " + str(total) + f" [{percent:3.2f}%]"
			)
		else:
			sys.stdout.write(
				'\r' + 80 * " " + '\r' + str(current)
			)

		sys.stdout.flush()

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StreamSimpleHuman(StreamBasic):
	'''Downloader with human readable progress message.'''
	_byte_t = {
		'kilo' : (1024, 'Kb'), 
		'mega' : (1024 ** 2, 'Mb'), 
		'giga' : (1024 ** 3, 'Gb'), 
		'tera' : (1024 ** 4, 'Tb'), 
		'peta' : (1024 ** 5, 'Pb'), 
		'exa' : (1024 ** 6, 'Ex') 
	}

	# -------------------------------------------------------------------------
	def __init__(self, srcurl, dst):
		super().__init__(srcurl, dst, True)

	#--------------------------------------------------------------------------
	def progress(self, current, total):
		'''Simple human readable, one-line progress output.'''
		if total is not None:
			percent = 100.0 * float(current) / float(total)
			sys.stdout.write(
				'\r' + 80 * " " + '\r' +
				self.size_str(current) + " / " + self.size_str(total) + f" [{percent:3.2f}%]"
			)
		else:
			sys.stdout.write(
				'\r' + 80 * " " + '\r' + self.size_str(current)
			)

		sys.stdout.flush()

	#------------------------------------------------------------------------------
	def size_str(self, x):
		'''
		Return string of integer in human readable format 1 Kb, Mb, Gb, Tb, Pb, Eb.
		'''
		if x < 1024:                    # Less than kilo-byte
			return str(x)
		elif x < 1048576:               # Less than mega-byte (1024 * 1024)
			xb = StreamSimpleHuman._byte_t['kilo']
		elif x < 1073741824:            # Less than giga-byte (1024 * 1024 *1024)
			xb = StreamSimpleHuman._byte_t['mega']
		elif x < 1099511627776:         # Less than tera-byte (1024 * 1024 * 1024 * 1024)
			xb = StreamSimpleHuman._byte_t['giga']
		elif x < 1125899906842624:
			xb = StreamSimpleHuman._byte_t['tera']
		elif x < 1152921504606846976:
			xb = StreamSimpleHuman._byte_t['peta']
		else:
			xb = StreamSimpleHuman._byte_t['exa']

		return f"{x / xb[0]:.2f} {xb[1]}"

