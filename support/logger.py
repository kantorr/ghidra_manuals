#! /usr/bin/env python3

'''
Simple stdio logger.

Date:
	26 April 2021
Author:
	kantorr
'''

import sys

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogColor:
	ACCENT_STRONG = '\033[95m'		# magenta
	ACCENT_LIGHT  = '\033[94m'		# ligth magenta
	SUCCESS       = '\033[92m'		# Green
	WARNING       = '\033[93m'		# Yellow
	ERROR         = '\033[91m'		# Red
	CLEAR         = '\033[0m'		# revert to normal light gray color

	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): return LogColor.ERROR + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): return LogColor.WARNING + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): return LogColor.SUCCESS + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): return LogColor.ACCENT_LIGHT + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): return LogColor.ACCENT_STRONG + msg + LogColor.CLEAR

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogSimple:
	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): return msg

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogStdIO:
	_log = LogSimple if sys.platform.startswith("win32") else LogColor

	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): print("ERROR: {}".format(LogStdIO._log.error(msg)))

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): print("WARNING: {}".format(LogStdIO._log.warning(msg)))

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): print(LogStdIO._log.success(msg))

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): print(LogStdIO._log.accent_light(msg))

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): print(LogStdIO._log.accent_strong(msg))
